/**
		Name: Tessa Herzberger
		SID: 200342876
		
		Institution: University of Regina
		Class: ENEL 487
		Professor: Karim Naqvi

		Assignment: #2
		Due: 4 October 2017
*/

#define NULL 0

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <limits.h>
#include <math.h>

using namespace std;

/**
	The Complex struct contains 8 feilds.
	- The character array of size 100 called "str" is used to hold the input from a file or user.
	- The character array of size 7 called "operation" that contains the operation selection that the user entered in str.
	- The float array of size 4 called "equation_value" that contains the numbers that the user entered in str.
	- The float array of size 2 called "solution" contains the soluton from the mathematic operation that the user entered in str.
	- The float variable called "magnitude" that is used to hold the magnitude of the real and imaginary components of the number.
	- The float variable called "theta" that is used to hold the angle of the real and imaginary components of the number.
	- The float array of size 2 called "inverse" that is used to hold calculated value of the inverse of the number entered.
	- The string variable called "complex_string" that is used to hold the input from a file or user.
*/
struct Complex
{
	char str[100];
	char operation[7];
	float equation_value[4];
	float solution[2];

	float magnitude;
	float theta;
	float inverse[2];
	string complex_string;
};


//Assignment 1
Complex divide(Complex entry);
Complex multiply(Complex entry);
Complex add(Complex entry);
Complex subtract(Complex entry);

//Assignment 2
Complex abs (Complex entry);
Complex arg (Complex entry);
Complex argDeg (Complex entry);
Complex inv (Complex entry);
Complex exp (Complex entry);


/**
	This is the main function. It asks for the input string in the form of "a 1 2 3 4",
	then obtains the first letter of the string and determines what mathematic operation
	will be performed (addition, subtraction, multiplication or division).
	After that, it displays the the output.
	This process repeats until 'q' is entered to exit the while loop and the program.
*/
int main()
{
	//Variable declarations
	Complex entry;
	int inc = 0;
	int space_counter = 0;
	int loop_counter = 0;
	int invalid = 0;
	char temp[100];
	int valid;

	//Initialize valid to 0.
	valid = 0;

	//Output the possible values.
	cerr << "Operations available:" << endl;
	cerr << "A z1 z2   :	calculate z1 + z2" << endl;
	cerr << "S z1 z2   :	calculate z1 - z2" << endl;
	cerr << "M z1 z2   :	calculate z1 * z2" << endl;
	cerr << "D z1 z2   :	calculate z1 / z2" << endl;
	cerr << "abs z1    :	calculate abs(z1)" << endl;
	cerr << "arg z1    :	calculate arg(z1) in radians" << endl;
	cerr << "argDeg z1 :	calculate abs(z1) in degrees" << endl;
	cerr << "exp z1    :	calculate exp(z1)" << endl;
	cerr << "inv z1    :	calculate 1/z1" << endl;
	cerr << "Q		   :	Quit" << endl;
	cerr << endl << "where each complex z is a pair of doubles." << endl;

	//This while loop continues until 'q' or 'Q' is entered.
	while (entry.operation != "q" || entry.operation != "Q")
	{
		//Clear the entry fields.
		entry.complex_string = "";
		for (int i = 0; i < 100; i++)
			entry.str[i] = NULL;
		for (int i = 9; i < 2; i++)
			entry.solution[i] = 0;
		for (int i = 9; i < 4; i++)
			entry.equation_value[i] = 0;
		

		//Reset both valid and space_counter to zero
		valid = 0;
		space_counter = 0;

		//Prompt for input.
		cerr << "Enter exp: " << endl;

		//Initialize all of the values in the char arrays temp and entry.str to null
		for(int i = 0; i < 100; i++)
		{
			entry.str[i] = NULL;
			temp[i] = NULL;
		}
		//Use the fgets function inputs the values from the temp character array into the entry.str string.
		if (fgets(temp, 100, stdin) != NULL)
		{
			for(int i = 0; i < 100; i++)
			{
				if (temp[i] != NULL)
					entry.str[i] += temp[i];
			}
		}

		//Count the number of white spaces in the character array entry.str
		//for (int i = 0; entry.str[i] != '\0'; i++)
		for (int i = 0; i < 100; i++)
		{
			if (entry.str[i] == ' ')
				space_counter++;
		}
		
		//Sets all values in entry.operation to the space character.
		for (int i = 0; i < 7; i++)
			entry.operation[i] = ' ';
		
		//Takes the characters up to the first space in the entry.str string and inputs it into the operation field of entry.
		while (entry.str[loop_counter] != ' ')
		{
			entry.operation[loop_counter] = entry.str[loop_counter];
			loop_counter++;
		}


		//Resets loop_counter to be equal to zero.
		loop_counter = 0;
		
		//Sets the string entry.complex_string to be equal to the the remaining entry.str char array.
		for(int i = 0; i < 100; i++)
		{
			if (entry.str[i] != NULL)
				entry.complex_string += entry.str[i];
		}

		//Erases the first operation selection portion of the input.
		while (entry.str[loop_counter] != ' ')
		{
			entry.complex_string = entry.complex_string.erase(0, 1);
			
			loop_counter++;
		}

		//Resets loop_counter to be equal to zero.
		loop_counter = 0;

		//Erase the left over first space.
		entry.complex_string = entry.complex_string.erase(0, 1);

		//Take the rest of the entry.str character array and inputs it into the equation_value array fields of entry.
		istringstream equation_stream(entry.complex_string);

		//Set three variables op_0, op_1 and op_2 to be equal to their respective entry.operation values.
		char op_0 = entry.operation[0];
		char op_1 = entry.operation[1];
		char op_2 = entry.operation[2];



		//Determine if there was an error entered in the initial input string.
		//If there is two or four spaces, the number of numbers and operations
		//entered is correct.
		if (space_counter == 2)
		{
			//Determine what operation was entered.
			switch (op_0)
			{
				//If the first character is 'a' or 'A',
				//then determine if the abs, arg, or argdeg
				//function was chosen, and take the two input numbers.
				//Then set valid equal to 1.
				case 'A':
				case 'a':
					if ((op_1 == 'b' || op_1 == 'B') && (op_2 == 's' || op_2 == 'S') && entry.operation[3] == ' ') 
					{
						equation_stream >> entry.equation_value[0] >> entry.equation_value[1];
						entry.equation_value[2] = 0;
						entry.equation_value[3] = 0;
						valid = 1;
					}
					else if ((op_1 == 'r' || op_1 == 'R') && (op_2 == 'g' || op_2 == 'G')) 
					{						
						if (((entry.operation[3] == 'd' || entry.operation[3] == 'D') && (entry.operation[4] == 'e' || entry.operation[4] == 'E') &&
							(entry.operation[5] == 'g' || entry.operation[5] == 'G'))	|| entry.operation[3] == ' ')
						{
							equation_stream >> entry.equation_value[0] >> entry.equation_value[1];
							entry.equation_value[2] = 0;
							entry.equation_value[3] = 0;
							valid = 1;
						}

						//If an incorrect operation was entered, set valid equal to 0.
						else
							valid = 0;
					}
					//If an incorrect operation was entered, set valid equal to 0.
					else
						valid = 0;
					break;
				//If the first character is 'i' or 'I',
				//then determine if the inv function was chosen,
				//and take the two input numbers.
				case 'I':
				case 'i':
					if ((op_1 == 'n' || op_1 == 'N') && (op_2 == 'v' || op_2 == 'V') && entry.operation[3] == ' ') 
					{
						equation_stream >> entry.equation_value[0] >> entry.equation_value[1];
						entry.equation_value[2] = 0;
						entry.equation_value[3] = 0;
						valid = 1;

						//Check for divide by zero error.
						if (entry.equation_value[0] == 0 && entry.equation_value[1] == 0)
						{	
							cerr << "Division by 0: Input line was: " << op_0 << op_1 << op_2 << " " << entry.complex_string << endl;
							valid = 0;
						}
					}
					//If an incorrect operation was entered, set valid equal to 0.
					else
						valid = 0;
					break;
				//If the first character is 'e' or 'E',
				//then determine if the exp function was chosen,
				//and take the two input numbers.
				case 'E':
				case 'e':
					if ((op_1 == 'x' || op_1 == 'X') && (op_2 == 'p' || op_2 == 'P') && entry.operation[3] == ' ') 
					{
						equation_stream >> entry.equation_value[0] >> entry.equation_value[1];
						entry.equation_value[2] = 0;
						entry.equation_value[3] = 0;
						valid = 1;
					}
					//If an incorrect operation was entered, set valid equal to 0.
					else
						valid = 0;
					break;
				//If an incorrect operation was entered,
				//Output an error statement.
				default:
					{
						loop_counter = 0;
			
						cerr << "Malformed Command: input line was: ";

						while (entry.operation[loop_counter] != ' ')
						{
							cout << entry.operation[loop_counter];
							loop_counter++;
						}
						cout << " " << entry.complex_string << endl;

						continue;
					}
					break;

			}
		}

		else if (space_counter == 4)
		{
			//Determine what operation was entered.
			switch (op_0)
			{
				//If a correct operation was entered,
				//take the values of the four input numbers.
				//Then set valid equal to 1.
				case 'a': 
				case 's':
				case 'm':
				case 'd':
				case 'A': 
				case 'S':
				case 'M':
				case 'D':
					if (op_1 == ' ')
					{
						equation_stream >> entry.equation_value[0] >> entry.equation_value[1] >> entry.equation_value[2] >> entry.equation_value[3];
						valid = 1;
						
						//Check for divide by zero error.
						if ((op_0 == 'd' || op_0 == 'D') && entry.equation_value[2] == 0 && entry.equation_value[3] == 0)
						{	
							cerr << "Division by 0: Input line was: " << op_0 << " " << entry.complex_string << endl;
							valid = 0;
						}
					}
					//If an incorrect operation was entered, set valid equal to 0.
					else
						valid = 0;
					break;
				//If an incorrect operation was entered,
				//Output an error statement.
				default:
					{
						loop_counter = 0;
						
						cerr << "Malformed Command: input line was: ";

						while (entry.operation[loop_counter] != ' ')
						{
							cout << entry.operation[loop_counter];
							loop_counter++;
						}
						cout << " " << entry.complex_string << endl;

						continue;
					}
					break;
			}
		}













		//If an incorrect number of operations or numbers was entered, output an error statement.
		else
		{
			loop_counter = 0;
			
			cerr << "Malformed Command: input line was: ";

			while (entry.operation[loop_counter] != ' ')
			{
				cout << entry.operation[loop_counter];
				loop_counter++;
			}
				cout << " " << entry.complex_string << endl;

			continue;
		}

		//Call the add function.
		if ((entry.operation[0] == 'a' || entry.operation[0] == 'A') && entry.operation[1] == ' ' && valid == 1)
		{
			entry = add(entry);

			//Output the solution.
			if (entry.solution[1] < 0)
				cout << entry.solution[0] << " - j " << (-1) * entry.solution[1] << endl;
			else
				cout << entry.solution[0] << " + j " << entry.solution[1] << endl;
		
		}

		//Call the subtract function.
		else if ((entry.operation[0] == 's' || entry.operation[0] == 'S') && valid == 1)
		{
			entry = subtract(entry);
			
			//Output the solution.
			if (entry.solution[1] < 0)
				cout << entry.solution[0] << " - j " << (-1) * entry.solution[1] << endl;
			else
				cout << entry.solution[0] << " + j " << entry.solution[1] << endl;
		}

		//Call the multiply function.
		else if ((entry.operation[0] == 'm' || entry.operation[0] == 'M') && valid == 1)
		{
			entry = multiply(entry);

			//Output the solution.
			if (entry.solution[1] < 0)
				cout << entry.solution[0] << " - j " << (-1) * entry.solution[1] << endl;
			else
				cout << entry.solution[0] << " + j " << entry.solution[1] << endl;	
	
		}

		//Call the divide function.
		else if ((entry.operation[0] == 'd' || entry.operation[0] == 'D') && valid == 1)
		{
			entry = divide(entry);

			//Output the solution.
			if (entry.solution[1] < 0)
				cout << entry.solution[0] << " - j " << (-1) * entry.solution[1] << endl;
			else
				cout << entry.solution[0] << " + j " << entry.solution[1] << endl;
		}

		//Call the abs function.
		else if (((entry.operation[0] == 'a' || entry.operation[0] == 'A') &&
			(entry.operation[1] == 'b' || entry.operation[1] == 'B') && (entry.operation[2] == 's' || entry.operation[2] == 'S')) && valid == 1) 
		{
			entry = abs(entry);
		}

		//Call the arg function.
		else if (((entry.operation[0] == 'a' || entry.operation[0] == 'A') &&
			(entry.operation[1] == 'r' || entry.operation[1] == 'R') && (entry.operation[2] == 'g' || entry.operation[2] == 'G'))
			&& entry.operation[3] == ' ' && valid == 1)
		{
			entry = arg(entry);

			//Output the solution.
			cout << entry.theta << endl;
		}

		//Call the argDeg function.
		else if (((entry.operation[0] == 'a' || entry.operation[0] == 'A') &&
			(entry.operation[1] == 'r' || entry.operation[1] == 'R') && (entry.operation[2] == 'g' || entry.operation[2] == 'G') &&
			(entry.operation[3] == 'd' || entry.operation[3] == 'D') && (entry.operation[4] == 'e' || entry.operation[4] == 'E') &&
			(entry.operation[5] == 'g' || entry.operation[5] == 'G')) && valid == 1)
		{
			entry = argDeg(entry);

			//Output the solution.
			cout << entry.theta << endl;
		}

		//Call the inv function.
		else if (((entry.operation[0] == 'i' || entry.operation[0] == 'I') &&
			(entry.operation[1] == 'n' || entry.operation[1] == 'N') && (entry.operation[2] == 'v' || entry.operation[2] == 'V')) && valid == 1)
		{
			entry = inv(entry);

			//Output the solution.
			if (entry.inverse[1] < 0)
				cout << entry.inverse[0] << " - j " << (-1) * entry.inverse[1] << endl;
			else
				cout << entry.inverse[0] << " + j" << entry.inverse[1] << endl;
		}

		//Call the exp function.
		else if (((entry.operation[0] == 'e' || entry.operation[0] == 'E') &&
			(entry.operation[1] == 'x' || entry.operation[1] == 'X') && (entry.operation[2] == 'p' || entry.operation[2] == 'V')
			&& entry.equation_value[2] == 0) && valid == 1)
		{
			entry = exp(entry);

			//Output the solution.
			if (entry.solution[1] < 0)
				cout << entry.solution[0] << " - j " << (-1) * entry.solution[1] << endl;
			else
				cout << entry.solution[0] << " + j " << entry.solution[1] << endl;
		}

		//Exit the program and the loop
		else if (entry.operation[0] == 'q' || entry.operation[0] == 'Q')
			return 0;
	}

	return 0;	
}

/** 
The add function takes a Complex variable,
adds the real and imaginary parts of the equation,
then returns the Complex variable to the main function.
*/
Complex add(Complex entry)
{

	entry.solution[0] = entry.equation_value[0] + entry.equation_value[2];
	entry.solution[1] = entry.equation_value[1] + entry.equation_value[3];

	return entry;
}

/** 
The multiply function takes a Complex variable,
multiplies the real and imaginary parts of the equation,
then returns the Complex variable to the main function.
*/
Complex multiply(Complex entry)
{
	entry.solution[0] = (entry.equation_value[0] * entry.equation_value[2]) - (entry.equation_value[1] * entry.equation_value[3]);
	entry.solution[1] = (entry.equation_value[0] * entry.equation_value[3]) + (entry.equation_value[1] * entry.equation_value[2]);

	return entry;
}

/** 
The subtract function takes a Complex variable,
subtracts the real and imaginary parts of the equation,
then returns the Complex variable to the main function.
*/
Complex subtract(Complex entry)
{
	entry.solution[0] = entry.equation_value[0] - entry.equation_value[2];
	entry.solution[1] = entry.equation_value[1] - entry.equation_value[3];

	return entry;
}

/** 
The divide function takes a Complex variable,
multiplies the numerator and denominator by the conjugate of the numerator,
then divides the real and imaginary parts of the equation.
After that, it returns the Complex variable to the main function.
*/
Complex divide(Complex entry)
{
	Complex temp_complex;

	temp_complex.solution[0] = (entry.equation_value[0] * entry.equation_value[2]) + (entry.equation_value[1] * entry.equation_value[3]);
	temp_complex.solution[1] = (entry.equation_value[1] * entry.equation_value[2]) - (entry.equation_value[0] * entry.equation_value[3]);

	entry.solution[0] = temp_complex.solution[0] / ((entry.equation_value[2] * entry.equation_value[2]) + (entry.equation_value[3] * entry.equation_value[3]));
	entry.solution[1] = temp_complex.solution[1] / ((entry.equation_value[2] * entry.equation_value[2]) + (entry.equation_value[3] * entry.equation_value[3]));

	return entry;
}

/** 
The abs function takes a Complex variable,
then computes the magnitude of the complex number.
After that, it returns the Complex variable to the main function.
*/
Complex abs (Complex entry)
{
	float magnitude = 0;

	magnitude = sqrt((entry.equation_value[0] * entry.equation_value[0]) + (entry.equation_value[1] * entry.equation_value[1]));

	cout << magnitude << endl; 
	
	return entry;
}

/** 
The arg function takes a Complex variable,
then computes the value of the complex number's angle, theta, in radians.
After that, it returns the Complex variable to the main function.
*/
Complex arg (Complex entry)
{
	entry.theta = atan2(entry.equation_value[1], entry.equation_value[0]);
	
	return entry;
}

/** 
The argDeg function takes a Complex variable,
then computes the value of the complex number's angle, theta, in radians.
It then converts theta from radians to degrees.
After that, it returns the Complex variable to the main function.
*/
Complex argDeg (Complex entry)
{
	const float CONVERT_RAD_TO_DEG = (float)(180.0/3.141593);

	entry = arg(entry);

	entry.theta = entry.theta * CONVERT_RAD_TO_DEG;

	return entry;
}

/** 
The inv function takes a Complex variable,
then computes the denominator for the inverse formula,
inverse = (x+jy)/(x-jy)(x+jy), then computes the denominator
for the equation, ((x^2)+((jy)^2)).
It then divides the two parts of the numerator by the denominator
to compute the inverse.
After that, it returns the Complex variable to the main function.
*/
Complex inv (Complex entry)
{
	float den = 0;

	den = (entry.equation_value[0] * entry.equation_value[0]) + (entry.equation_value[1] * entry.equation_value[1]);

	entry.inverse[0] = entry.equation_value[0]/den;
	entry.inverse[1] = (entry.equation_value[1] * -1)/den;

	return entry;
}

/** 
The exp function takes a Complex variable,
then computes the value of e^(a + jb) using the following formula
e^(a + jb) = (e^a)(cos(c) + j*sin(c))
After that, it returns the Complex variable to the main function.
*/
Complex exp (Complex entry)
{
	float e = (float)2.71828;
	float temp = 0;
	
	entry.solution[0] = 0;
	entry.solution[1] = 0;

	temp = pow(e, entry.equation_value[0]);
	entry.solution[0] = temp * (cos(entry.equation_value[1]));
	entry.solution[1] = temp * (sin(entry.equation_value[1]));

	return entry;
}