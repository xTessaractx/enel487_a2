Program: ENEL487_A2.cpp
Name: Tessa Herzberger
SID: 200342876
Institution: University of Regina
Class: ENEL 487
Professor: Karim Naqvi
Assignment: #1
Due: 4 October 2017


GENERAL USAGE NOTES:
- This program takes an input string in one of two forms:
	- char_array float float float float
	- char_array float float
  and calculates a arithmetic operation with two real numbers and two
  imaginary numbers.
- The arithmetic operation is determined by the char.
	- 'a' or 'A' for addition.
	- 's' or 'S' for subtraction.
	- 'm' or 'M' for multiply.
	- 'd' or 'D' for division.
	- 'abs', 'ABS', or any combination of the corresponding uppercase and lowercase letters for absolute.
	- 'arg', 'ARG', or any combination of the corresponding uppercase and lowercase letters for argument.
	- 'argdeg', 'ARGDEG', or any combination of the corresponding uppercase and lowercase letters for argument degree.
	- 'inv', 'INV', or any combination of the corresponding uppercase and lowercase letters for inverse.
	- 'exp', 'EXP', or any combination of the corresponding uppercase and lowercase letters for exponent.
	- 'q' or 'Q' to quit the program.
- The first and third float numbers entered are always the real values.
- The second and fourth float numbers are always the imaginary values.

STRUCT:
- A struct named "Complex" was created. It contains a eight fields which have been modified from assignment #1 to suit the needs of assignment #2:
	- The character array of size 100 called "str" is used to hold the input from a file or user.
	- The character array of size 7 called "operation" that contains the operation selection that the user entered in str.
	- The float array of size 4 called "equation_value" that contains the numbers that the user entered in str.
	- The float array of size 2 called "solution" contains the soluton from the mathematic operation that the user entered in str.
	- The float variable called "magnitude" that is used to hold the magnitude of the real and imaginary components of the number.
	- The float variable called "theta" that is used to hold the angle of the real and imaginary components of the number.
	- The float array of size 2 called "inverse" that is used to hold calculated value of the inverse of the number entered.
	- The string variable called "complex_string" that is used to hold the input from a file or user.

FUNCTIONS:
- main
	- The main function asks for the input string in either the form
	  "operation number number" or "operation number number number number",
	  then obtains the operation char array value of the string and determines what
	  mathematic operation will be performed (addition, subtraction, multiplication,
	  division, absolute, argument, argument degree, inverse and exponent).
	  After that, it displays the the output.
	- This process repeats until 'q' is entered to exit the while loop and the program.  
- add
	- The add function takes a Complex variable, adds the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.
- subtract
	- The subtract function takes a Complex variable, subtracts the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.
- multiply
	- The add function takes a Complex variable, adds the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.
- divide
	- The add function takes a Complex variable, adds the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.
- absolute
	- The abs function takes a Complex variable,
	  then computes the magnitude of the complex number.
	  After that, it returns the Complex variable to the main function.
- argument	  
	- The arg function takes a Complex variable,
	  then computes the value of the complex number's angle, theta, in radians.
	  After that, it returns the Complex variable to the main function.
- argument degree
	- The argDeg function takes a Complex variable,
	  then computes the value of the complex number's angle, theta, in radians.
	  It then converts theta from radians to degrees.
	  After that, it returns the Complex variable to the main function.
- inverse
	- The inv function takes a Complex variable,
	  then computes the denominator for the inverse formula,
	  inverse = (x+jy)/(x-jy)(x+jy), then computes the denominator
	  for the equation, ((x^2)+((jy)^2)).
	  It then divides the two parts of the numerator by the denominator
	  to compute the inverse.
	  After that, it returns the Complex variable to the main function.
- exponent
	- The exp function takes a Complex variable,
	  then computes the value of e^(a + jb) using the following formula
	  e^(a + jb) = (e^a)(cos(c) + j*sin(c))
	  After that, it returns the Complex variable to the main function.

COMPILING AND RUNNING:
- In order to compile and run this program in command line, use the following lines:
	  g++ -o A1 ENEL487_A1.cpp
	  ./A1
- To compile and run this program in batch mode in command line, use the following lines:
  (NOTE: input.txt and output.txt will be changed to the desired input and output files.
  The input file needs to be added to the same directory as the .cpp file)
	  g++ -o A1 ENEL487_A1.cpp
	  ./A1 <input.txt >output.txt